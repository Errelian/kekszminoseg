﻿#pragma once

#include <iostream>
#include <string>
#include <algorithm>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>


using namespace std;
using namespace cv;
//a filapatheket a felhasználónak kell átírnia

void createHisto(const Mat& img, Mat& hiszto) //hisztogram készítése, a dokumentáció kódja alapján
{
    Mat imgHSV;
    cvtColor(img, imgHSV, COLOR_BGR2HSV);

    int h_bins = 50, s_bins = 60;
    int histSize[] = { h_bins, s_bins };        //forrás: https://docs.opencv.org/3.4/d8/dc8/tutorial_histogram_comparison.html

    float h_ranges[] = { 0, 180 };
    float s_ranges[] = { 0, 256 };

    const float* ranges[] = { h_ranges, s_ranges };

    int channels[] = { 0, 1 };

    calcHist(&imgHSV, 1, channels, Mat(), hiszto, 2, histSize, ranges, true, false);
}

void prepare(const Mat& img, vector<Point>& Contour, Mat& histo, double& hArea ) //előkíszit mindent: visszadja a legnagyobb kontúrt és hisztogrammot készít
{

    Mat gray, thresh, morph, cnt, roi;

    cnt = img.clone();

    cvtColor(img, gray, COLOR_BGR2GRAY);

    threshold(gray, thresh, 0, 255, THRESH_BINARY | THRESH_OTSU);

    morphologyEx(thresh, morph, MORPH_OPEN, getStructuringElement(MORPH_ELLIPSE, Size(11, 11)));

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;

    findContours(morph.clone(), contours, hierarchy, RetrievalModes::RETR_TREE, ContourApproximationModes::CHAIN_APPROX_NONE); //eddig minden standard, küszöbölés, morfológia, kontúrkezelés

    double max = 0;
    double temp;
    Scalar crackerClr(0, 255, 0);
    Scalar holeClr(0, 0, 255);
    int id = 0;

    for (size_t j = 0; j < contours.size(); j++)        //kiválasztom a legnagyobb kontúrt mert valószínüleg az lesz a keksz, és azt használom referenciának
    {
        drawContours(cnt, contours, j, crackerClr, 2);
        temp = contourArea(contours[j]);
        if (temp > max)
        {
            max = temp;
            id = j;
        }
    }

    Rect r;
    r = boundingRect(contours[id]);
    roi = img(r);       //kiválasztom a Region of Interestet, a kekszet

    createHisto(roi, histo);

    Contour = contours[id];

    double tempH, maxH = 0;

    for (size_t j = 0; j < hierarchy.size(); j++)       //kiválasztja a legnagyobbat a keksz belső, gyermek kontúrjai közül
    {
        if (hierarchy[j][3] == id)      //hogyha a keksz külső kontúrja szülője
        {
            drawContours(cnt, contours, j, holeClr, 2);

            tempH = contourArea(contours[j]);

            if (tempH > maxH)
            {
                maxH = tempH;
            }
        }
    }

    hArea = maxH;
}

bool assertQuality(const Mat& img, const vector<Point>& cCont, const Mat& histo)
{
    Mat hiszto;

    vector<Point> Contour;

    double areaH;

    prepare(img, Contour, hiszto, areaH);

    double delta = matchShapes(Contour, cCont, CONTOURS_MATCH_I2, 0);        //a 3. típus bizonyult a legjobbnak az én esetemhez

    cout << "Image deviation from contour: " << to_string(delta);

    bool cont;

    if (delta < 0.70)   //külső kontúr delta, 0.0 és 1.0 közé eshet
    {
        cout << "  NOMINAL\n";
        cont = true;
    }
    else
    {
        cout << "  DEVIANT\n";
        cont = false;
    }

    double histoDelta = compareHist(histo, hiszto, 3);

    cout << "Image histogram deviation: " << to_string(histoDelta);

    bool hist, hole;

    if (histoDelta < 0.5)   //hisztogrammok deltája 0.0 és 1.0 közé eshet
    {
        cout << " NOMINAL\n";
        hist = true;
    }
    else
    {
        cout << " DEVIANT\n";
        hist = false;
    }

    cout << "Largest internal hole size: " << to_string(areaH);

    if (areaH < 600)        //legnagyobb belső lyuk területe, tapasztalat alapján a standard keksz belső lyukai 400 pixelnél nem kéne nagyobbak legyenek
    {
        cout << " NOMINAL \n";
        hole = true;
    }
    else
    {
        cout << " DEVIANT\n";
        hole = false;
    }


    if (hist && cont && hole)       //döntő logika, egyértelmű
    {
        cout << "Overall assertion: NOMINAL\n\n";
        return true;
    }
    else
    {
        cout << "Overall assertion: DEVIANT\n\n";
        return false;
    }
};

int main()
{
        cout << "Reading control image: \n" ;

        String cPath = "C:\\KEPFEL2\\good (6).jpg";

        Mat control = imread(cPath, IMREAD_UNCHANGED);    //referencia kép, minden mást ehhez hasonlítok, erősen hasraütés-szerűen lett kiválasztva, a pathet a felhasználó felelőssége beállítani
        
        if (control.empty())
        {
            cout << "ERROR: Couldn't read the control image at: " << cPath << "\n";
        }
        else
        {
            cout << "Succesfully read the control image at : " << cPath << "\n";

            Mat hiszto;

            vector<Point> Contour;

            cout << "Preparing Control image: \n\n";

            double areaC;

            prepare(control, Contour, hiszto, areaC);


            double FN = 0, FP = 0, TN = 0, TP = 0, PU = 0, NU = 0, U = 0; //a statisztikához


            cout << "Reading the good images: \n";
            for (size_t i = 1; i < 7; i++)
            {
                String path = "C:\\KEPFEL2\\good (" + to_string(i) + ").jpg";
                Mat img = imread(path, IMREAD_UNCHANGED);
                if (img.empty())
                {
                    cout << "Error reading at: " + path + "\n";
                }
                else
                {
                    cout << "Succesful read at: " + path + "\n";
                    cout << "Asserting image:" + to_string(i) << "\n";
                    bool asd = assertQuality(img, Contour, hiszto); //test

                    if (asd)
                    {
                        TN++;
                        U++;     //a belső statisztikához használt counterek növelése
                        NU++;
                    }
                    else
                    {
                        FP++;
                        U++;
                        NU++;
                    }
                }
            }
            cout << "Reading the faulty images: \n";
            for (size_t i = 1; i < 7; i++)
            {
                String path = "C:\\KEPFEL2\\bad (" + to_string(i) + ").jpg";
                Mat img = imread(path, IMREAD_UNCHANGED);
                if (img.empty())
                {
                    cout << "Error reading at: " + path + "\n";
                }
                else
                {
                    cout << "Succesful read at: " + path + "\n";
                    cout << "Asserting image:" + to_string(i) << "\n";
                    bool asd = assertQuality(img, Contour, hiszto); //test

                    if (asd)
                    {
                        FN++;
                        U++;
                        PU++;
                    }
                    else
                    {
                        TP++;
                        U++;
                        PU++;
                    }
                }
            }
            double FNp, FPp, TNp, TPp;
            double spec, sens;

            spec = (TN / NU) * 100;
            sens = (TP / PU) * 100;

            FNp = (FN / U) * 100;
            FPp = (FP / U) * 100;
            TNp = (TN / U) * 100;
            TPp = (TP / U) * 100;

            cout << "False negative percentage: " << FNp << "%\n";
            cout << "False positive percentage: " << FPp << "%\n";
            cout << "True negative percentage: " << TNp << "%\n";       //ezek a statisztikák nem teljesen reprezentatívak, mivel a hibás keksz képeknek szándékosan a nehezek lettek kiválogatva, az algoritmus hatékonyságának tesztelése érdekében
            cout << "True positive percentage: " << TPp << "%\n";

            cout << "Specificity: " << spec << "%\n";
            cout << "Sensitivity: " << sens << "%\n";

            //köszönöm a figyelmet
            //további magyarázat: a .ppt-ben
        }
}

